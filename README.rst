Demo für Code Checks in gitlab-ci.yml
-------------------------------------

Bandit:
=======

https://pypi.org/project/bandit/

Findet Security Probleme.
In diesem Beispiel die Nutzung von assert. Hier handelt es sich zwar nur um einen Test, aber man sollte assert in bestimmten Fällen nicht nutzen,
die Erklärung findet sich hier: https://hackernoon.com/10-common-security-gotchas-in-python-and-how-to-avoid-them-e19fbe265e03
Im Test ist dies unproblematisch, wahrscheinlich sollte man die Tests von der Überprüfung ausschließen.

    .. image:: images/bandit.PNG

Radon:
======

https://pypi.org/project/radon/

Berechnet Metriken z.B. für die Komplexität. Das F vor der Zeile steht bspw. für Funktion und dann kommt der Name der Funktion und dann folgt die Bewertung.
A ist sehr gut, wobei mein Beispiel auch eine Schachtelung weniger vertragen könnte. Ich hatte gehofft, so bekommt man hier mal auf die Schnelle ein negativeres Ergebnis. :-)

    .. image:: images/radon.PNG

Flake8:
=======

https://pypi.org/project/flake8/

Überprüft bspw. Codingstyle nach PEP8.
In diesem Fall habe ich extra sehr hässlich programmiert und Leerzeichen relativ willkürlich genutzt, um die Ausgabe zu demonstrieren.

    .. image:: images/f8.PNG



